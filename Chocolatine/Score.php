<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Chocolatine?</title>
    <link rel="stylesheet" href="Sidebar.css" />
    <link rel="stylesheet" href="Score.css" />
    <link rel="icon" href="Images/Icone.png" />
  </head>
  <body>
    <!-- Mise en place de la barre latérale -->
    <div id='mySidebar' class='sidebar'>
      <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
      <a href="Histoire.html">Histoire</a>
      <a href="Carte.php">Démarrer</a>
      <a href="Score.php">Classement</a>
    </div>

    <!-- Création du bouton Menu -->
    <div id="bouton">
      <button class="openbtn" onclick="openNav()">&#9776; Menu </button>
    </div>

    <!-- Création du tableau des scores -->
    <div id='main'>
    <?php
    echo "<div id='entete'><h1 id='titre'>Les plus désireux de trouver la solution sont :</h1></div>";
    //Connection à la base de données
      $servername  = "localhost";  // nom du serveur
      $username_bdd = "root";      // nom d'utilisateur
      $password_bdd = "root";      // mot de passe
      $db_name = "croissant21";    // nom de la base de données


      // Elaboration de la connexion
      $conn = mysqli_connect($servername, $username_bdd, $password_bdd, $db_name);

      // Vérifie la connection
      if (!$conn) {
          die("Connection failed: " . mysqli_connect_error());
      }

    //Limiter le classement aux 10 premiers joueurs
    $result = mysqli_query($conn,"SELECT pseudo,resultat FROM score ORDER BY resultat DESC LIMIT 10");

    //Création du tabeau
    echo "<div id='contenu'><table id='table_score'>
    <tr>
    <th>Pseudo</th>
    <th>Points</th>
    </tr>";
    while($row = mysqli_fetch_array($result))
    {
    echo "<tr>";
    echo "<td>" . $row['pseudo'] . "</td>";
    echo "<td>" . $row['resultat'] . "</td>";
    echo "</tr>";
    }
    echo "</table></div>";

    mysqli_close($conn);
    ?>
    </div>

    <!-- Appel des javascripts -->
    <script type="text/javascript" src="Sidebar.js"></script>

  </body>
</html>
