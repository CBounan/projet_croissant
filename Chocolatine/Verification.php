<?php
session_start();

//Connection à la base de données
  $servername  = "localhost";  // nom du serveur
  $username_bdd = "root";      // nom d'utilisateur
  $password_bdd = "root";      // mot de passe
  $db_name = "croissant21";    // nom de la base de données


  // Elaboration de la connection
  $conn = mysqli_connect($servername, $username_bdd, $password_bdd, $db_name);

  // Vérifie la connexion
  if (!$conn) {
      die("Connection failed: " . mysqli_connect_error());
  }

//S'il l'utilisateur a entré un pseudo
if(isset($_POST['utilisateur'])){
  $utilisateur=$_POST['utilisateur'];
  $autrejoueur = mysqli_query($conn,"SELECT pseudo FROM joueur WHERE pseudo='$utilisateur'");

  //Si aucun autre joueur n'a ce pseudo
  if(mysqli_num_rows($autrejoueur)==0){
    //Ajout du pseudo à la base de données et création d'un id pour le joueur
    $longueur = mysqli_num_rows(mysqli_query($conn,"SELECT id_joueur FROM joueur"))+1;
    $sql = "INSERT INTO joueur (id_joueur, pseudo) VALUES ('$longueur','$utilisateur')";

    if (mysqli_query($conn, $sql)) {
      //Création d'une session pour le joueur
      $_SESSION['utilisateur']=$utilisateur;
      echo '<script>window.location.href = "Histoire.html";</script>';
    }
    else{
      echo '<script>alert("Erreur pas d\'ajout");</script>';
      echo '<script>window.location.href = "Jeu.html";</script>';
    }
  }
  //Si un utilisateur a déjà ce pseudo
  else{
    //Redemander le pseudo
    echo '<script>alert("Le nom d\'utilisateur est déjà utilisé.");</script>';
    echo '<script>window.location.href = "Jeu.html";</script>';
    }
}


?>
