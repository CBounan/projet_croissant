//Récupération du bouton Voir mon score
fin = document.getElementById("fin");

//Si le joueur clique sur Voir mon score
fin.addEventListener('click', score_table);

//Fonction permettant d'afficher la page
function score_table() {
  //Récupération de la variable du score stockée en vocal
  var temps_rest = sessionStorage.getItem("minut");
  var donnee = 'tempsfinal=' + temps_rest;

  //Mise à jour de la table des scores
  fetch('Tablescore.php', {
    method: 'post',
    body: donnee,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
  .then(result=>{
    //Rediriger vers le tableau des scores
    window.location.href="Score.php";
  })
}
