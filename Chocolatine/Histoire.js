//Récupération des div à l'aide des id
var suivant = document.getElementById("suivant");
var main = document.getElementById("main");
var intro = document.getElementById("intro");
var p1 = document.getElementById("p1");
var p2 = document.getElementById("p2");
var question = document.getElementById("question");
var image1 = document.getElementById("imagePains");

//Si on clique sur le bouton suivant, executer affiche_suite
suivant.addEventListener('click', affiche_suite);

//Affiche la deuxième page de l'histoire
function affiche_suite(){
  //Remplace toutes les div par d'autres div
  p1.remove();
  p2.remove();
  suivant.remove();
  intro.innerHTML = "Chaque année de plus en plus de personnes sont touchées par cette question. L'épidémie atteindra bientôt le monde entier !";
  question.innerHTML = "Serez-vous capable de relever le défi et répondre enfin à cette question ?";
  image1.setAttribute("src", "Images/illustration-chocolatine.jpg");
  image1.setAttribute("alt", "Image de dispute");
  var newDiv = document.createElement("p");
  newDiv.setAttribute("id", "Comment_demarrer");
  document.getElementById("main").appendChild(newDiv);
  newDiv.innerHTML="Cliquer sur le bouton Démarrer dans le menu pour commencer le jeu.";
}
