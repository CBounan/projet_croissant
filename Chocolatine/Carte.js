//Variables du Timer
var tempsfinal = new Date().getTime()+1000*60*5;
var minuteur = document.getElementById('minuteur');
var heure;
var laps;
var minutes;
var secondes;

//Fonction qui met en place un compte à rebours de 5min
function Timer(){
  heure = new Date().getTime();
  laps = tempsfinal-heure;
  minutes = Math.floor((laps % (1000 * 60 * 60)) / (1000 * 60));
  secondes = Math.floor((laps % (1000 * 60)) / 1000);
  minuteur.innerHTML = "Il reste " + minutes + " min " + secondes + " sec avant la fin du jeu.";
  if (secondes==00){
    //Affiche en rouge toutes les minutes
    minuteur.style.color = 'red';
    minuteur.style.fontWeight = 'bold';
  }
  if(secondes!=00) {
    minuteur.style.color = '';
    minuteur.style.fontWeight = '';
  }
  if(laps<=1000){
    //Affiche la page Perdu si le jeu n'est pas fini au bout de 5min
    document.location.href="Perdu.html";
  }
  setTimeout(Timer, 1000);
}
Timer();

//Affichage de la carte centrée sur Paris
var coord = [48.86, 2.34];
var mymap = L.map('mapid').setView(coord, 12);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);

//Initialisation des variables
var galette;
var skinner;
var code;
var fourferme;
var fourouvert;
var cle;
var boulangerie;
var viennoiseries;

var imgalette;
var imskinner;
var imcode;
var imfourferme;
var imfourouvert;
var imcle;
var imboulangerie;
var imviennoiseries;

var ouvre_boul='OFF';
var premiertotem=0;
var deuxiemetotem=0;

var emplacement2=document.getElementById('emplacement2');
var cleinv=document.getElementById('Cle_inv');


var list_obj= [];
var list_imobjet = [];
var galette;

//Icone typique
var Image = L.Icon.extend({
options: {
    shadowUrl: '',
    iconSize:     [40, 40],
    shadowSize:   [50, 64],
    iconAnchor:   [22, 94],
    shadowAnchor: [4, 62],
    popupAnchor:  [-3, -76]
}
});

//Fonction d'affichage des objets de la base de données
function affiche_icone(){
  //Appel au fichier connect accédant à la table des objets de la base de données
  fetch('connect.php')
    .then(r => r.json())
    .then(r => {
      //Parcours de la base de donnees
      var i;
      for (i = 0; i < r.length; i++) {
        Image = L.Icon.extend({options: {
            // Taille de l'icone
            iconSize:     [r[i].largeur, r[i].hauteur],
            // Point de l'image par rapport auquel l'image va être centrée
            iconAnchor:   [r[i].largeur/2, r[i].hauteur/2],
        }});

        //Ajustement en fonction des objets (les commentaires de l'image galette valent pour toutes les images)
        if (r[i].nom == "galette") {
          //liste correspondant aux paramètres de latitude, de longitude,
          //de niveau de zoom,des instruction et de la visiblitité de l'objet
          galette = [r[i].latitude,r[i].longitude, r[i].zoom, r[i].instruction, true];
          //Création du marqueur de l'image
          imgalette = L.marker([r[i].latitude,r[i].longitude], {icon: new Image({iconUrl: r[i].image})});
          //Liste des objets avec leurs attibuts
          list_obj.push(galette);
          //Liste des marqueurs
          list_imobjet.push(imgalette);
        };

        if (r[i].nom == "skinner") {
          skinner = [r[i].latitude,r[i].longitude, r[i].zoom, r[i].instruction, true];
          imskinner = L.marker([r[i].latitude,r[i].longitude], {icon: new Image({iconUrl: r[i].image})});
          list_obj.push(skinner);
          list_imobjet.push(imskinner);
        };

        if (r[i].nom == "dgplein") {
          dgplein = [r[i].latitude,r[i].longitude, r[i].zoom, r[i].instruction, true];
          imdgplein = L.marker([r[i].latitude,r[i].longitude], {icon: new Image({iconUrl: r[i].image})});
          list_obj.push(dgplein);
          list_imobjet.push(imdgplein);
        };

        if (r[i].nom == "dgmoyen") {
          dgmoyen = [r[i].latitude,r[i].longitude, r[i].zoom, r[i].instruction, false];
          imdgmoyen = L.marker([r[i].latitude,r[i].longitude], {icon: new Image({iconUrl: r[i].image})});
          list_obj.push(dgmoyen);
          list_imobjet.push(imdgmoyen);
        };

        if (r[i].nom == "dgvide") {
          dgvide = [r[i].latitude,r[i].longitude, r[i].zoom, r[i].instruction, false];
          imdgvide = L.marker([r[i].latitude,r[i].longitude], {icon: new Image({iconUrl: r[i].image})});
          list_obj.push(dgvide);
          list_imobjet.push(imdgvide);
        };

        if (r[i].nom == "code") {
          code = [r[i].latitude,r[i].longitude, r[i].zoom, r[i].instruction, false];
          imcode = L.marker([r[i].latitude,r[i].longitude], {icon: new Image({iconUrl: r[i].image})});
          list_obj.push(code);
          list_imobjet.push(imcode);
        };

        if (r[i].nom == "fourferme") {
          fourferme = [r[i].latitude,r[i].longitude, r[i].zoom, r[i].instruction, true];
          imfourferme = L.marker([r[i].latitude,r[i].longitude], {icon: new Image({iconUrl: r[i].image})});
          list_obj.push(fourferme);
          list_imobjet.push(imfourferme);
        };

        if (r[i].nom == "fourouvert") {
          fourouvert = [r[i].latitude,r[i].longitude, r[i].zoom, r[i].instruction, false];
          imfourouvert = L.marker([r[i].latitude,r[i].longitude], {icon: new Image({iconUrl: r[i].image})});
          list_obj.push(fourouvert);
          list_imobjet.push(imfourouvert);
        };

        if (r[i].nom == "cle") {
          cle = [r[i].latitude,r[i].longitude, r[i].zoom, r[i].instruction, false];
          imcle = L.marker([r[i].latitude,r[i].longitude], {icon: new Image({iconUrl: r[i].image})});
          list_obj.push(cle);
          list_imobjet.push(imcle);
        };

        if (r[i].nom == "boulangerie") {
          boulangerie = [r[i].latitude,r[i].longitude, r[i].zoom, r[i].instruction, true];
          imboulangerie = L.marker([r[i].latitude,r[i].longitude], {icon: new Image({iconUrl: r[i].image})});
          list_obj.push(boulangerie);
          list_imobjet.push(imboulangerie);
        };

        if (r[i].nom == "totem1") {
          totem1 = [r[i].latitude,r[i].longitude, r[i].zoom, r[i].instruction, true];
          imtotem1 = L.marker([r[i].latitude,r[i].longitude], {icon: new Image({iconUrl: r[i].image})});
          list_obj.push(totem1);
          list_imobjet.push(imtotem1);
        };

        if (r[i].nom == "totem2") {
          totem2 = [r[i].latitude,r[i].longitude, r[i].zoom, r[i].instruction, true];
          imtotem2 = L.marker([r[i].latitude,r[i].longitude], {icon: new Image({iconUrl: r[i].image})});
          list_obj.push(totem2);
          list_imobjet.push(imtotem2);
        };

        if (r[i].nom == "viennoiseries") {
          viennoiseries = [r[i].latitude,r[i].longitude, r[i].zoom, r[i].instruction, false];
          imviennoiseries = L.marker([r[i].latitude,r[i].longitude], {icon: new Image({iconUrl: r[i].image})});
          list_obj.push(viennoiseries);
          list_imobjet.push(imviennoiseries);
        };
    };

    //Affichage des objets en fonction du niveau de zoom
    mymap.on('zoomend', function() {
      //Récupération du zoom courant
      var currentZoom = mymap.getZoom();
      for (i=0; i<list_obj.length; i++){
        if (list_obj[i][4]==true) {
          //Si le zoom est suffisamment proche
          if (currentZoom >= list_obj[i][2]) {
            if (mymap.hasLayer(list_imobjet[i])) {
              //Si le marqueur est déjà là, il ne se passe rien
            }
            else{
              //Sinon ajouter le markeur
              mymap.addLayer(list_imobjet[i]);
            }
          }
          //Si le zoom est trop loin
          if (currentZoom < list_obj[i][2]) {
            //Si le marqueur est sur la carte
            if (mymap.hasLayer(list_imobjet[i])) {
              //Enlever le marqueur
              mymap.removeLayer(list_imobjet[i]);
            }
          }
        }
      }
    });

    // Ajout d'un popup avec les instructions
    for (i=0;i<list_obj.length; i++){
      let obj=list_imobjet[i];
      let instructi=list_obj[i][3];
      //Si l'objet a une instruction
      if(!!list_obj[i][3]){
        //si ce n'est pas la boulangerie ni les totems
        if(list_obj[i] != boulangerie){
          if(list_obj[i] != totem1){
            if(list_obj[i] != totem2){
              list_imobjet[i].addEventListener('click', function(){
                //Ajouter l'instruction dans un popup
                Ajout_Instr(obj,instructi);
              })
            }
          }
        }
      }
    };

    //Récupérer les totems
    imtotem1.addEventListener('click', function(){
      //Au deuxième clic ajout du totem à l'inventaire
      if(premiertotem==1){
        //Enlever le marqueur de totem quand on clique dessus
        mymap.removeLayer(imtotem1);
        list_obj[10][4]=false;
        //Créer l'entité image totem1inv
        var totem1inv = document.createElement("img");
        totem1inv.setAttribute("src", "Images/Totem1.png");
        totem1inv.setAttribute("height", "60");
        totem1inv.setAttribute("width", "100");
        totem1inv.setAttribute("alt", "Premier Totem");
        totem1inv.setAttribute("id", "totem1");
        totem1inv.setAttribute("class", "img_inv");
        //Ajouter totem1inv à une des div de l'inventaire
        document.getElementById("emplacement2").appendChild(totem1inv);
      }
      else{
        //La première fois ajout de l'instruction
        Ajout_Instr(imtotem1,totem1[3]);
        premiertotem+=1;
      }
    });

    imtotem2.addEventListener('click', function(){
      //Au deuxième clic ajout du totem à l'inventaire
      if(deuxiemetotem==1){
        //Enlever le marqueur de totem quand on clique dessus
        mymap.removeLayer(imtotem2);
        list_obj[11][4]=false;
        //Créer l'entité image totem1inv
        var totem2inv = document.createElement("img");
        totem2inv.setAttribute("src", "Images/Totem2.png");
        totem2inv.setAttribute("height", "100");
        totem2inv.setAttribute("width", "100");
        totem2inv.setAttribute("alt", "Deuxième Totem");
        totem2inv.setAttribute("id", "totem2");
        totem2inv.setAttribute("class", "img_inv");
        //Ajouter totem1inv à une des div de l'inventaire
        document.getElementById("emplacement3").appendChild(totem2inv);
      }
      else{
        //La première fois ajout de l'instruction
        Ajout_Instr(imtotem2,totem2[3]);
        deuxiemetotem+=1;
      }
    });

    //Tuer le monstre
    imdgplein.addEventListener('click', function(){
      //Si on clique sur le monstre afficher l'image où il perd une vie et enlever
      //l'image courante
      mymap.removeLayer(imdgplein);
      mymap.addLayer(imdgmoyen);
      list_obj[2][4]=false;
      list_obj[3][4]=true;
    });

    imdgmoyen.addEventListener('click', function(){
      //Si on clique sur le monstre afficher l'image où il perd une vie et enlever
      //l'image courante
      mymap.removeLayer(imdgmoyen);
      mymap.addLayer(imdgvide);
      list_obj[3][4]=false;
      list_obj[4][4]=true;
    });

    imdgvide.addEventListener('click', function(){
      //Si on clique sur le monstre afficher le code et enlever l'image courante
      mymap.removeLayer(imdgvide);
      mymap.addLayer(imcode);
      list_obj[4][4]=false;
      list_obj[5][4]=true;
    });

    //Ajout du formulaire au coffre
    imfourferme.addEventListener('click', function(){
      imfourferme.unbindPopup();
      //Création du formulaire dans un popup pour ouvrir le coffre
      form="<form method='get' id='formcode'><div><label>Code: <input id='code' type='text'></label></div><div><input type='submit' value='Vérifier'></div></form>";
      imfourferme.addTo(mymap).bindPopup(form).openPopup();
      formulaire=document.getElementById('formcode');
      //Soumettre le formulaire
      formulaire.addEventListener('submit',function (event){
        event.preventDefault();
        if(verif_code(event)){
          //Si le code est vérifié, ajouter les marqueurs du four ouvert et de la clé
          //et enlever le marqueur du four fermé
          imfourferme.addTo(mymap).bindPopup(form).closePopup();
          mymap.removeLayer(imfourferme);
          mymap.addLayer(imfourouvert);
          mymap.addLayer(imcle);
          list_obj[6][4]=false;
          list_obj[7][4]=true;
          list_obj[8][4]=true;
        }
      });
    });

    //Ajout de la clé à l'inventaire
    imcle.addEventListener('click', function(){
      //Enlever le marqueur de clé quand on clique dessus
      mymap.removeLayer(imcle);
      list_obj[8][4]=false;
      //Créer l'entité image cleinv
      var cleinv = document.createElement("img");
      cleinv.setAttribute("src", "Images/Cle.png");
      cleinv.setAttribute("height", "100");
      cleinv.setAttribute("width", "100");
      cleinv.setAttribute("alt", "Cle");
      cleinv.setAttribute("id", "Cle_inv");
      cleinv.setAttribute("class", "img_inv");
      //Ajouter cleinv à une des div de l'inventaire
      document.getElementById("emplacement1").appendChild(cleinv);
      //Fonction de selection de clé dans l'inventaire
      cleinv.addEventListener("click", function(){
        var empl=document.getElementById("emplacement1");
        //Si la clé est sélectionnée dans l'inventaire
        if (ouvre_boul=="ON"){
          ouvre_boul="OFF";
          //Suppression du contour rouge
          empl.setAttribute("style", "border: solid #492a03;");
        }
        //Si la clé n'est pas sélectionnée dans l'inventaire
        else{
          ouvre_boul="ON";
          //Ajout d'un contour rouge
          empl.setAttribute("style", "border: solid red;");
        }
      });
    });

    //Déblocage de la boulangerie grâce à la clé
    imboulangerie.addEventListener('click',function(){
      //Si la clé est sélectionnée dans l'inventaire
      if (ouvre_boul=="ON"){
        //Enlever la clé de l'inventaire
        document.getElementById("Cle_inv").style.display='none';
        //Supprimer le marqueur de la boulangerie et ajouter le marqueur des viennoiseries
        mymap.removeLayer(imboulangerie);
        mymap.addLayer(imviennoiseries);
        list_obj[9][4]=false;
        list_obj[10][4]=true;
      }
      //Si la clé n'est pas sélectionnée dans l'inventaire
      else{
        //Ajouter l'instruction associée à la boulangerie
        Ajout_Instr(imboulangerie,boulangerie[3]);
      }
    });

    //Detection de fin de partie quand on clique sur les viennoiseries
    imviennoiseries.addEventListener('click', function(){
      var tps=minutes*60+secondes;
      //Si les totems sont récupérés cela rajoute 50 points pour chaque totem
      if(premiertotem==1){
        tps+=50;
      }
      if(deuxiemetotem==1){
        tps+=50;
      }
      //Ajouter le temps en secondes en stockage local de l'onglet  actif
      sessionStorage.clear();
      sessionStorage.setItem("minut", ""+tps+"");
      //Redirection vers la page de fin
      document.location.href="Fin.html";
      });

  })

};

affiche_icone();

//Véfifie s'il y a des instructions et ajoute un popup si c'est le cas
function Ajout_Instr(imobjet, instruction){
  //Ajout des instructions liées à l'objet dans un popup
  imobjet.unbindPopup();
  imobjet.bindPopup(instruction).openPopup();
  }

//Fonction vérifiant le code du coffre-four
function verif_code(event){
  event.preventDefault();
  code=document.getElementById("code").value;
  //Si le code est correct on obtient true, sinon false
  if (code=='56238'){
    return true;
  }
  else{
    return false;
  }
};
