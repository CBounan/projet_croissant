<?php
//Connection à la base de données
  $servername  = "localhost";  // nom du serveur
  $username_bdd = "root";      // nom d'utilisateur
  $password_bdd = "root";      // mot de passe
  $db_name = "croissant21";    // nom de la base de données


  // Elaboration de la connexion
  $conn = mysqli_connect($servername, $username_bdd, $password_bdd, $db_name);

  // Vérifie la connection
  if (!$conn) {
      die("Connection failed: " . mysqli_connect_error());
  }

// Selectionner les objets
  $requete = "SELECT * FROM objet";
  $result = mysqli_query($conn, $requete);
  if ($result) {
    $Table_objet=[];
    while ($ligne = mysqli_fetch_assoc($result)) {
      $Table_objet[] = $ligne;
    }
  }

//Renvoie le tableau des objets
echo json_encode($Table_objet);


?>
