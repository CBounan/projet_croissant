-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  Jeu 05 nov. 2020 à 10:23

--
-- Base de données :  `escape_game`
--
--
-- --------------------------------------------------------

--
-- Structure de la table `objet`
--

CREATE TABLE `objet` (
  `id_objet` int(30) UNSIGNED NOT NULL,
  `nom` varchar(255) NOT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `zoom` float DEFAULT '12',
  `image` varchar(255) NOT NULL,
  `largeur` int(11) DEFAULT '100',
  `hauteur` int(11) DEFAULT '100',
  `instruction` varchar(255) NOT NULL	
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
INSERT INTO `objet` (`id_objet`, `nom`, `latitude`, `longitude`, `zoom`, `image`, `largeur`, `hauteur`, `instruction`) VALUES
(1, 'galette', 48.86, 2.34, 10, 'Images/Galette_des_rois.png', 70, 120, 'Bonjour, La réponse à votre question se trouve dans la boulangerie place de la Répubique de Panama à Paris. Vous devriez aller voir le chef Skinner dans son restaurant chez Gusto rue de Charenton pour plus d\'informations. Bon courage !'),
(2, 'skinner', 48.845718, 2.379962, 16, 'Images/chef_skinner.png', 50, 120, 'Hey ! Un coffre se trouve au niveau du four de Paris entre Reims et Verdun mais il est fermé. Allez donc faire un tour du côté de la Tour Eiffel ... heu non ... au sacré coeur ... à moins que ça soit du côté du château de Vincennes. Bref essayez !'),
(3, 'dgplein', 48.842696, 2.434689, 16, 'Images/Dragon_plein.png', 110, 130, NULL),
(4, 'dgmoyen', 48.842696, 2.434689, 16, 'Images/Dragon_Moyen.png', 110, 130, NULL),
(5, 'dgvide', 48.842696, 2.434689, 16, 'Images/Dragon_vide.png', 110, 130, NULL),
(6, 'code', 48.842696, 2.434689, 16, 'Images/Etiquette.png', 120, 120,'56238'),
(7, 'fourferme', 49.186175, 4.939041, 16, 'Images/Four_ferme.png', 70, 120, NULL),
(8, 'fourouvert', 49.186175, 4.939041, 16, 'Images/Four_ouvert.png', 70, 120, NULL),
(9, 'cle', 49.186000, 4.939110, 16, 'Images/Cle.png', 15, 25, NULL),
(10, 'boulangerie', 48.846932, 2.306979, 16, 'Images/Boulangerie.png', 120, 120, 'Il faut la clé'),
(11, 'totem1', 48.88677, 2.343022, 16, 'Images/Totem1.png', 100, 60, 'Je suis un totem ! Cliquez à nouveau sur moi pour me récupérer.'),
(12, 'totem2', 48.858228, 2.294512, 16, 'Images/Totem2.png', 100, 100, 'Je suis un totem ! Cliquez à nouveau sur moi pour me récupérer.'),
(13, 'viennoiseries', 48.846932, 2.306979, 16, 'Images/Viennoiseries.png', 100, 60, NULL);
-- --------------------------------------------------------

--
-- Structure de la table `joueur`
--

CREATE TABLE `joueur` (
  `id_joueur` int(10) UNSIGNED NOT NULL,
  `pseudo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `joueur` (`id_joueur`, `pseudo`) VALUES
(1, 'Momo'),
(2, 'Balthus'),
(3, 'Dchiley');


-- --------------------------------------------------------

--
-- Structure de la table `score`
--

CREATE TABLE `score` (
  `id_score` int(10) UNSIGNED NOT NULL,
  `pseudo` varchar(255) NOT NULL,
  `resultat` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Déchargement des données de la table `score`
--

INSERT INTO `score` (`id_score`, `pseudo`, `resultat`) VALUES
(1, 'Momo', 100),
(2, 'Balthus', 120),
(3, 'Dchiley', 110);
