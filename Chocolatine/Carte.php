<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
      <meta charset="utf-8">
      <title>Chocolatine?</title>
      <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
     integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
     crossorigin=""/>
     <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
    integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
    crossorigin=""></script>
    <link rel="stylesheet" href="Sidebar.css" />
    <link rel="stylesheet" href="Carte.css" />
    <link rel="icon" href="Images/Icone.png" />
  </head>

  <body>
    <div id="entete">
      <h2>Pain au chocolat ou chocolatine ?</h2>
    </div>
    <div id="carte">
      <!-- Mise en place du minuteur -->
      <p id="minuteur"></p>
      <!-- Mise en place des éléments de la carte -->
      <div id="mapid"></div>

    </div>
    <!-- Mise en place de l'inventaire -->
    <div id="inventaire">
      <div id="titre">
        <p >Inventaire</p>
      </div>
      <div id="depot">
        <div class="carre" id="emplacement1"></div>
        <div class="carre" id="emplacement2"></div>
        <div class="carre" id="emplacement3"></div>
      </div>
    </div>



    <!-- Appel des javascripts -->
    <script type="text/javascript" src="Carte.js"></script>
    <script type="text/javascript" src="Sidebar.js"></script>
  </body>
</html>
