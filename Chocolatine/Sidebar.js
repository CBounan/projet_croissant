//Mettre la taille de la sidebar à 250px et la marge à gauche du contenu de la page à 250px
function openNav() {
  document.getElementById("mySidebar").style.width = "250px";
  document.getElementById("main").style.marginLeft = "250px";
}

//Mettre la taille de la sidebar à 0px et la marge à gauche du contenu de la page à 0px
function closeNav() {
  document.getElementById("mySidebar").style.width = "0";
  document.getElementById("main").style.marginLeft = "0";
}
